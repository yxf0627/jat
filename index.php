<?php 
$sys = isset($_GET['sys']) ? $_GET['sys'] : '';
$v = isset($_GET['v']) ? $_GET['v'] : '';
$img_arr = [];
//获得token
function getToken() {
    $data = 'verify_name=getPushToken&verify_password=a4407266ea6b0c11ae8b658351f7203a';
    $res = httpsPost('https://debug-gc.program.com.tw/apis/v1.1/pushToken',$data);
    $res = json_decode($res, true);
    if($res['success'] != 1) {
        return '';
    }
    return $res['retval'];
}
//获得图片
function getImages() {
	$sys = isset($_GET['sys']) ? $_GET['sys'] : '';
	$v = isset($_GET['v']) ? $_GET['v'] : '';
    $model_key = $sys.'_'.$v;
    $token = getToken();
    $data = 'token='.$token.'&model_key='.$model_key;
    $res = httpsPost('https://debug-gc.program.com.tw/apis/Marls/device/image', $data);
    $res = json_decode($res, true);
    if($res['success'] != 1) {
        return [];
    }
    if(!empty($res['retval'])) {
    	//重新排序
	    $order_no = array_column($res['retval'],'order_no');
		array_multisort($order_no,SORT_ASC,$res['retval']);
    }
    return $res['retval'];
}
function httpsPost($url,$data){ // 模拟提交数据函数
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo; // 返回数据，json格式
}
$img_arr = getImages();
$img_count = count($img_arr)-1;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<title>骑行 App 保活设置教程</title>
<link rel="stylesheet" href="css/swiper.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/index.css?t=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
<meta name="imagemode" content="force">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="">
<link rel="apple-touch-icon-precomposed" href="">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<script src="js/jquery.min.js"></script>
</head>
<body>
	<section class="wrap page01">
		<header>
	    	<ul>
				<li class="header-icon"> 
					<span>帮助</span>
				</li>
			</ul>
		</header>
		<div class="title1"><label><?=$sys.'_'.$v ?></label></div>
		<!--
		<div class="swiper-box">
			<div class="swiper-container swiper11">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="con con-active">ColorOS V5</div>
					</div>
					<div class="swiper-slide">
						<div class="con">ColorOS V2.1</div>
					</div>
					<div class="swiper-slide">
						<div class="con">ColorOS V2.1</div>
					</div>
					<div class="swiper-slide">
						<div class="con">ColorOS V3.1</div>
					</div>
				</div>
			</div>
		</div>
	-->
		<ul class="tishi-panel">
			<li> 
				<span class="tishi2-title">请按照以下操作进行设置</span>
			</li>
		</ul>
		<div class="swiper-box">
			<div class="swiper-container swiper12">
				<div class="swiper-wrapper">
					<?php foreach($img_arr as $key => $val) {?>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="<?=$val['image_url']?>" alt="">
							</a>
						</div>
						<?php if($img_count == $key) {?>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/tm_icon.png" alt="">
							</a>
						</div>
						<?php }else {?>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
						<?php }?>
					</div>
					<?php }?>
					<!--
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/2.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/3.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/4.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/5.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/6.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/right.png" alt="">
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="con2">
							<a href="javascript:void(0)">
								<img src="img/guide/7.png" alt="">
							</a>
						</div>
						<div class="con3">
							<a href="javascript:void(0)">
								<img src="img/tm_icon.png" alt="">
							</a>
						</div>
					</div>
				-->
				</div>
			</div>
		</div>
	</section>
	<script src="js/slide.js"></script>
	<script src="js/polyfill.min.js"></script>
	<script src="js/swiper.min.js"></script>
	<script src="js/main.js"></script>
	<script>
		//解决最后一个高度不一致
		//var con2_height = $('.con2').height()+'px';
		//$('.con2').attr('style', 'height:'+con2_height);
		//第一个滑动
		var swiper = new Swiper('.swiper11', {
			slidesPerView: 3.2,
			spaceBetween: 0,
			initialSlide:0,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			autoplay: {
				delay: 50000000,
				disableOnInteraction: false,
			},
		});
		//第2个滑动
		var swiper = new Swiper('.swiper12', {
			slidesPerView: 1.1,
			spaceBetween: 0,
			initialSlide:0,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			autoplay: {
				delay: 50000000,
				disableOnInteraction: false,
			},
		});
		$(function() {
			$('.con').click(function() {
				$('.con').removeClass('con-active');
				$(this).addClass('con-active');
			});
		});
	</script>
</body>
</html>