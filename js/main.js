/**
 * Created by UICUT.com on 2016/12/31.
 * Contact QQ: 215611388
 */
(function () {
  var p = 0,
      t = 0;
  $(window).scroll(function (e) {
    p = $(this).scrollTop();

    if (t <= p) {
      //向下滚
      $("header,.h98").css("top", "-100%");
    } else {
      //向上滚
      $("header,.h98").css("top", "0%");
    }

    setTimeout(function () {
      t = p;
    }, 10);
  });
})();

(function ($) {
  "use strict";

  (function () {
    // 全局事件
    // 通用：弹框关闭
    $(document).on('click', '.alert .btn-close,.alert .over-close,.alert .js_btn-cancle', function (event) {
      event.preventDefault();
      $(this).parents(".alert").fadeOut('300', function () {
        $(this).removeClass('show');
      });
    }); // 验证码发送

    function timeClock(cls) {
      var _this = cls;

      if (_this.hasClass('disabled')) {
        return false;
      } else {
        var clock = function clock() {
          _this.text("重新发送(" + i + ")");

          i--;

          if (i < 0) {
            _this.removeClass('disabled');

            i = 59;

            _this.text("发送验证码(60)");

            clearInterval(int);
          }
        };

        _this.addClass('disabled');

        var i = 59;
        var int = setInterval(clock, 1000);
        return false;
      }
    } // 发送验证码


    $("body").on('click', '.btn-yzm', function (event) {
      event.preventDefault();
      timeClock($(this));
    }); // tab切换

    $("body").on('click', '.demoTest .filter .item', function (event) {
      event.preventDefault();

      if (!$(this).hasClass('on')) {
        $(this).addClass('on').siblings().removeClass('on');
        $('.demoTest .details .con').css({
          display: 'none'
        }).eq($(this).index()).css({
          display: 'block'
        });
      }
    });
  })();
})(jQuery);

TouchSlide({
  slideCell: "#js_banner",
  titCell: ".hd ul",
  mainCell: ".bd ul",
  effect: "leftLoop",
  autoPlay: true,
  autoPage: true,
  switchLoad: "_src"
});
TouchSlide({
  slideCell: "#tabBox1",
  endFun: function endFun(i) {
    var bd = document.getElementById("tabBox1-bd");
    bd.parentNode.style.height = bd.children[i].children[0].offsetHeight + 1 + "px";
    if (i > 0) bd.parentNode.style.transition = "200ms";
  }
});